package client;

/**
 * Created by appadmin on 5/6/2017.
 */
public class SignRequestBody {

    private String base64;

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }
}
