package client;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.ProxySelector;
import java.net.URI;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.*;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.CertificateUtil;
import com.itextpdf.text.pdf.security.CrlClient;
import com.itextpdf.text.pdf.security.CrlClientOnline;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.OcspClientBouncyCastle;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.itextpdf.text.pdf.security.TSAClient;
import com.itextpdf.text.pdf.security.TSAClientBouncyCastle;

import java.security.cert.CertificateException;

import sun.misc.BASE64Decoder;
import sun.security.mscapi.SunMSCAPI;
import sun.security.x509.X500Name;

import java.security.KeyStore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sun.security.provider.certpath.OCSP;

@SuppressWarnings("restriction")
public class DigiSignUtility {

	static X509Certificate[] certificateChain = null;
	static Key privateKey = null;
	static String alias = null;
	private static Logger log = LoggerFactory.getLogger(DigiSignUtility.class);

	private static Object invokeGetter(Object instance, String methodName)
			throws NoSuchMethodException, IllegalAccessException,
			InvocationTargetException {
		Method getAlias = instance.getClass().getDeclaredMethod(methodName);
		getAlias.setAccessible(true);
		return getAlias.invoke(instance);
	}

	private static boolean isSelfSigned(X509Certificate cert)
			throws CertificateException, NoSuchAlgorithmException,
			NoSuchProviderException {
		try {
			// Try to verify certificate signature with its own public key
			PublicKey key = cert.getPublicKey();
			cert.verify(key);
			return true;
		} catch (SignatureException sigEx) {
			// Invalid signature --> not self-signed
			return false;
		} catch (InvalidKeyException keyEx) {
			// Invalid key --> not self-signed
			return false;
		}
	}

	private static void writeByteArraysToFile(String fileName, byte[] content)
			throws IOException {
		File file = new File(fileName);
		BufferedOutputStream writer = new BufferedOutputStream(
				new FileOutputStream(file));
		writer.write(content);
		writer.flush();
		writer.close();
	}

	public static String signpdf(String reqBody, String issuedby,
			String[] issuedto, String sampleurl, String good,
			String revoked, String unknown, String tempdirectory,String issuedby2)
			throws JSONException {
		log.info("issuedBy properties file :" + issuedby);
		JSONObject signDoc = new JSONObject();
		KeyStore ks;
		SunMSCAPI providerMSCAPI;
		X509Certificate[] certificateChain = null;
		Key privateKey = null;
		String alias;
		String base64string = null;
		String property;
		Collection entries;
		List l = null;
		try {
			System.setProperty("java.net.useSystemProxies", "true");
            l = ProxySelector.getDefault().select(new URI(sampleurl));

            if (l != null) {
                for (Iterator iter = l.iterator(); iter.hasNext();) {
                    java.net.Proxy proxy = (java.net.Proxy) iter.next();
                    log.info("proxy hostname : " + proxy.type());
                    InetSocketAddress addr = (InetSocketAddress) proxy.address();
                    if (addr == null) {
                    	log.info("No Proxy");
                    } 
                    else {
                    	log.info("proxy hostname : " + addr.getHostName());
                    	log.info("proxy port : " + addr.getPort());
                    }
                }
            }

			String bs = reqBody;
			providerMSCAPI = new SunMSCAPI();
			Security.addProvider(providerMSCAPI);
			ks = KeyStore.getInstance("Windows-MY");
			ks.load(null, null);
			log.info("KeyStore loading");
			Field spiField = KeyStore.class.getDeclaredField("keyStoreSpi");
			spiField.setAccessible(true);
			KeyStoreSpi spi = (KeyStoreSpi) spiField.get(ks);
			Field entriesField = spi.getClass().getSuperclass()
					.getDeclaredField("entries");
			entriesField.setAccessible(true);
			Object get = entriesField.get(spi);
			if (get instanceof Collection) {
				log.info("collection");
				entries = (Collection) entriesField.get(spi);
			} else {
				log.info("hashmap");
				entries = ((HashMap) entriesField.get(spi)).values();
			}

			log.info("check the entries :" + entries.size());
			if (!entries.isEmpty()) {
				for (Object entry : entries) {
					alias = (String) invokeGetter(entry, "getAlias");
					privateKey = (Key) invokeGetter(entry, "getPrivateKey");
					certificateChain = (X509Certificate[]) invokeGetter(entry,
							"getCertificateChain");

					
					OcspClient ocspClient = new OcspClientBouncyCastle();
					TSAClient tsaClient = null;
					String issuedTo = null, issuedBy = null;
					X509Certificate cert = null;
					log.info("certificateChain length:"
							+ certificateChain.length);
					if (certificateChain.length == 4) {
						for (int i = 0; i < certificateChain.length; i++) {
							log.info("inside for loop certificateChain length:"
									+ certificateChain.length);
							cert = (X509Certificate) certificateChain[i];
							X500Name asX500Name = X500Name
									.asX500Name(certificateChain[0]
											.getSubjectX500Principal());
							X500Name asX500Name1 = X500Name
									.asX500Name(certificateChain[1]
											.getSubjectX500Principal());
							issuedTo = asX500Name.getOrganization();
							issuedBy = asX500Name1.getCommonName();
							log.info("issuedTo :" + issuedTo);
							log.info("issuedBy :" + issuedBy);
							boolean[] keyUsage = cert.getKeyUsage();
							String tsaUrl = CertificateUtil.getTSAURL(cert);
							if (tsaUrl != null) {
								tsaClient = new TSAClientBouncyCastle(tsaUrl);
								break;
							}
							List<CrlClient> crlList = new ArrayList<CrlClient>();
							crlList.add(new CrlClientOnline(certificateChain));

//							OCSP.RevocationStatus.CertStatus certStatus = null;
//							// checking the revocation status
//							if (issuedby.equals(issuedBy)) {
//								OCSP.RevocationStatus check = OCSP.check(
//										certificateChain[0],
//										certificateChain[1]);
//								certStatus = check.getCertStatus();
//								log.info("certStatus :" + certStatus.toString());
//							}

							boolean flag = isSelfSigned(certificateChain[0]);

							//if (good.equals(certStatus.toString())) {
								if (keyUsage[0]) { // checking if the
													// certificate keyusage is
													// Digital Signature
									if (flag) {
										signDoc.put(
												"base64string",
												"This is not corporate issued certificate, Please use corporate issued valid certificate.");
										signDoc.put("statusCode", "100");
									}// end of if
									else {
										for (int j = 0; j < issuedto.length; j++) {
											if (issuedto[j].contains(issuedTo)
													&& (issuedby
															.equals(issuedBy)||issuedby2
															.equals(issuedBy)) ){
												property = System
														.getProperty(tempdirectory);
												BASE64Decoder decoder = new BASE64Decoder();
												byte[] FileByte = decoder
														.decodeBuffer(bs);
												writeByteArraysToFile(property
														+ "_unsigned.pdf",
														FileByte);

												// Creating the reader and the
												// stamper∂
												PdfReader reader = new PdfReader(
														property
																+ "_unsigned.pdf");
												FileOutputStream os = new FileOutputStream(
														property
																+ "_signed.pdf");
												PdfStamper stamper = PdfStamper
														.createSignature(
																reader, os,
																'\0');
												log.info("pdf reader called");
												// Creating the appearance
												PdfSignatureAppearance appearance = stamper
														.getSignatureAppearance();
												appearance
														.setAcro6Layers(false);
												appearance
														.setVisibleSignature(
																new Rectangle(
																		100,
																		100,
																		300, 0),
																reader.getNumberOfPages(),
																"sig1");
												// Creating the signature
												ExternalSignature pks = new PrivateKeySignature(
														(PrivateKey) privateKey,
														DigestAlgorithms.SHA256,
														providerMSCAPI
																.getName());
												ExternalDigest digest = new BouncyCastleDigest();
												MakeSignature
														.signDetached(
																appearance,
																digest,
																pks,
																certificateChain,
																crlList,
																ocspClient,
																tsaClient,
																0,
																MakeSignature.CryptoStandard.CMS);
												InputStream docStream = new FileInputStream(
														property
																+ "_signed.pdf");
												byte[] encodeBase64 = Base64
														.encodeBase64(IOUtils
																.toByteArray(docStream));
												base64string = new String(
														encodeBase64);
												signDoc.put("base64string",
														base64string);
												signDoc.put("statusCode", "101");
												log.info("sending base64 ended");
												return signDoc.toString();
											}// end of org if
											else {
												signDoc.put(
														"base64string",
														"This is not corporate issued certificate, Please use corporate issued valid certificate.");
												signDoc.put("statusCode", "102");
											}
										}
									}// end of else block
								}// closing if true k[0]
								else {
									log.info("Not a DS certificate");
								}// closing else k[0]
//							}// if Revocation status is good close
//							else if (revoked.equals(certStatus.toString())) {
//								signDoc.put("base64string",
//										"Please insert a valid token.");
//								signDoc.put("statusCode", "105");
//							}// if Revocation status is revoked close
//							else if (unknown.equals(certStatus.toString())) {
//								signDoc.put("base64string",
//										"Please contact your token provider.");
//								signDoc.put("statusCode", "106");
//							}// if Revocation status is unknown close

						} // end of for certificate chain
					}// end of chain length if
					else {
						if (base64string == null) {
							signDoc.put("base64string",
									"Certificate Token used is not valid, please insert valid token.");
							signDoc.put("statusCode", "107");
						}
					}

				}// end of for object entries
			}// end if collection
			else {
				signDoc.put("base64string",
						"Certificate Token used is not valid, please insert valid token.");
				signDoc.put("statusCode", "103");

			}// entries else block
		} catch (Exception ex) {
			if (ex.getMessage().contains("An internal error occurred.")) {
				signDoc.put("base64string", "");
				signDoc.put("statusCode", "108");
				StringWriter writer = new StringWriter();
				PrintWriter pw = new PrintWriter(writer);
				ex.printStackTrace(pw);
				log.warn("Exception :"+writer.toString());
			} else {
				signDoc.put(
						"base64string",
						"Internal Server Error: Please try after sometime. If error continues, please contact helpdesk");
				signDoc.put("statusCode", "104");
				StringWriter writer = new StringWriter();
				PrintWriter pw = new PrintWriter(writer);
				ex.printStackTrace(pw);
				log.warn("Exception :"+writer.toString());
			}
		}
		return signDoc.toString();
	}

}
