package com.tecnics.ong.client;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.annotation.PostConstruct;

import client.DigiSignUtility;
import client.SignRequestBody;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Configuration
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@EnableAutoConfiguration
@PropertySource({"classpath:default.properties"})
@ComponentScan
public class OngcClientApplication {

private Logger log = LoggerFactory.getLogger(OngcClientApplication.class);

@Value("${sign.issuedBy}")
private String issuedBy;

//added for class2
@Value("${sign.issuedby}")
private String issuedby;

@Value("${sign.issuedTo}")
private String[] issuedTo;

@Value("${sample.url}")
private String sampleurl;

@Value("${token.good}")
private String good;

@Value("${token.revoked}")
private String revoked;

@Value("${token.unknown}")
private String unknown;

@Value("${temp.directory}")
private String tempdirectory;

	@RequestMapping(path = "/sign",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
	public String sign(@RequestBody SignRequestBody signRequestBody) {
		log.info("Starting");
		String result = "";
		try {
            result = DigiSignUtility.signpdf(signRequestBody.getBase64(),issuedBy,issuedTo,sampleurl,good,revoked,unknown,tempdirectory,issuedby);
        } catch (Exception e) {
			StringWriter writer = new StringWriter();
        	PrintWriter pw = new PrintWriter(writer);
        	e.printStackTrace(pw);
        	log.warn("Exception :"+writer.toString());
		}

		return result;
	}
	
	
	@RequestMapping(path = "/clientstatus",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public String sign() {
		log.info("Checking the server status");
		return "Digital Signature Client is up and running.";
	}
	
	@PostConstruct
    public void postConstruct() {
		log.info("OngcClientApplication started: " + issuedBy);
    }

	public static void main(String[] args) throws Exception {
		SpringApplication.run(OngcClientApplication.class, args);
	}
	
}
